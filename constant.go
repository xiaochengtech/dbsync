package dbsync

const (
	// 数据库类型
	SqlTypeMySQL  = "mysql"  // MySQL类型数据库
	SqlTypeSqlite = "sqlite" // SQLite类型数据库
)
