package dbsync

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"testing"
)

// 测试MySQL
func TestMySQL(t *testing.T) {
	var err error
	defer func() {
		if err != nil {
			t.Error(err)
		}
	}()
	// 来源数据库
	fromDB, err := gorm.Open("mysql", "root:123456@tcp(192.168.0.2:3307)/test?charset=utf8&parseTime=True&loc=Asia%2fChongqing")
	if err != nil {
		return
	}
	defer fromDB.Close()
	// 目标数据库
	toDB, err := gorm.Open("mysql", "root:123456@tcp(192.168.0.2:3308)/test?charset=utf8&parseTime=True&loc=Asia%2fChongqing")
	if err != nil {
		return
	}
	defer toDB.Close()
	// 开启流程测试
	err = GeneralTest(fromDB, toDB, SqlTypeMySQL)
}
