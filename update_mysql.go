package dbsync

import (
	"fmt"
	"strings"
)

// 通用插入数据
func updateMySQL(
	db SQLCommon,
	tableName string,
	data [][]interface{},
	options UpdateOptions,
) (err error) {
	// 列名和索引的映射关系
	mapItem := make(map[string]int)
	for i, fieldName := range options.Columns {
		mapItem[fieldName] = i
	}
	i, columnsLen := 0, len(options.Columns)
	for k := range options.FixedFields {
		mapItem[k] = columnsLen + i
		i++
	}
	mapItemLen := len(mapItem)
	// 生成唯一键或主键的判断映射
	uniqueMap := make(map[string]bool)
	for _, k := range options.UniqueFields {
		uniqueMap[k] = true
	}
	// 生成时间列的判断映射
	timeMap := make(map[string]bool)
	for _, k := range options.TimeFields {
		timeMap[k] = true
	}
	// 生成SQL语句的列名、问号、值列表
	columns := make([]string, mapItemLen)
	updateColumns := make([]string, 0)
	questions := make([]string, mapItemLen)
	for k, num := range mapItem {
		columns[num] = k
		if uniqueMap[k] != true {
			updateColumns = append(updateColumns, fmt.Sprintf("%s=VALUES(%s)", k, k))
		}
		questions[num] = "?"
	}
	colStr, questionStr, updateStr := strings.Join(columns, ","), fmt.Sprintf("(%s)", strings.Join(questions, ",")), strings.Join(updateColumns, ",")
	dataLen := len(data)
	values := make([]interface{}, mapItemLen*dataLen)
	allQuestions := make([]string, dataLen)
	for k, dataItem := range data {
		for fieldName, num := range mapItem {
			index := mapItemLen*k + num
			if num >= columnsLen {
				values[index] = options.FixedFields[fieldName]
			} else {
				if timeMap[fieldName] == true {
					values[index] = convertTimeType(dataItem[num])
				} else {
					values[index] = dataItem[num]
				}
			}
		}
		allQuestions[k] = questionStr
	}
	allQuestionStr := strings.Join(allQuestions, ",")
	// 生成并执行SQL语句
	sqlStr := fmt.Sprintf("INSERT INTO %s (%s) VALUES %s ON DUPLICATE KEY UPDATE %s",
		tableName, colStr, allQuestionStr, updateStr)
	_, err = db.Exec(sqlStr, values...)
	return
}
